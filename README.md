Nombro / Nombro.eu

Nombro.eu aims to be a mathematical database. 

Technical information:

 - .NET Core 2 Projects
 - .NET ASP Stats Page
 - NPoco 
 - Math.NET
 - MySQL / MariaDB Connection

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Currently this is a unclean PoC with WIP.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

This is a MonoDevelop project - i didn't test any other IDE. 
Compiling with "dotnet build" works.

Commands are: 
 -  primes - start prime classification
 -  sieve - start sieveing numbers
 -  sieveall - start sieveing all tables
 -  missingprimes - check for missing primes in
 -  stats - start statistics update
 -  compress - compress tables 
 -  deflate - deflate tables (Integer text > varchar) 

 Options are:
 -  primes [startpos]
 -  sieve <startpos> <endpos>
 -  sieveall - none
 -  missingprimes [startpos]
 -  stats - none 
 -  compress - none 
 -  deflate - none 


If you like to support - mail to: nombro@lmnt.one
