﻿using System.Numerics;
using TableInfo = Nombro.Objects.TableInfo;

namespace Nombro.Objects
{

    public class TableAndName
    {
        public TableInfo tableInfo;
        public BigInteger number;
    }

    public class NumberAndCount
    {
        public string number;
        public int count;
    }

}

namespace Nombro.Objects.V2
{

    public class TableAndName
    {
        public TableInfo tableInfo;
        public BigInteger number;
    }

    public class NumberAndCount
    {
        public string number;
        public int count;
    }

}
