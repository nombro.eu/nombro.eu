﻿using System;

namespace Nombro.Objects
{

    public class MachineStats
    {
        public int Id { get; set; }
        public string MachineType { get; set; }
        public string MachineName { get; set; }
        public string FreeRam { get; set; }
        public string UsedCpu { get; set; }
        public DateTime LastWrite { get; set; }
    }


}

namespace Nombro.Objects.V2
{

    public class MachineStats
    {
        public int Id { get; set; }
        public string MachineType { get; set; }
        public string MachineName { get; set; }
        public string FreeRam { get; set; }
        public string UsedCpu { get; set; }
        public DateTime LastWrite { get; set; }
    }


}