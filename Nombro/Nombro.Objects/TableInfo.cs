﻿using System;

namespace Nombro.Objects
{
    public class TableInfo
    {
        public string DB { get; set; }
        public string Table { get; set; }
        public int CountNumbers { get; set; }
        public int CountPrimes { get; set; }

    }

    public class TableAccess
    {
        public int Id { get; set; }
        public string DB { get; set; }
        public string TableName { get; set; }
        public DateTime LastWrite { get; set; }

    }

    public class TableConfirmed
    {
        public int Id { get; set; }
        public string DB { get; set; }
        public string TableName { get; set; }
        public bool PrimesConfirmed { get; set; }

    }



}