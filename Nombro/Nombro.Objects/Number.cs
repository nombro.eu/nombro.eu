﻿using NPoco;
using Newtonsoft.Json;
using System;

namespace Nombro.Objects
{
    public class Number
    {
        public int Id { get; set; }
        public string Integer { get; set; }
        public bool IsPrime { get; set; }
        public string LowestFactorization { get; set; }
    }
}

namespace Nombro.Objects.V2
{
    public class Number
    {
        [JsonIgnore]
        [Ignore]
        private int _id;


        [JsonProperty(PropertyName = "I")]
        public int NId { get => _id; set => _id = value; }

        [JsonIgnore]
        [Ignore]
        public string TableName { get; set; } = "";

        [JsonIgnore]
        [Ignore]
        public string Integer
        {
            get
            {
                string fullNumber = NId.ToString();
                if (TableName != "N0MI")
                {
                    string Millions = TableName.Replace("N", "").Replace("MI", "");
                    fullNumber = Millions + string.Join("0", new string[7 - fullNumber.Length]) + fullNumber;
                }

                return fullNumber;
            }
            set
            {

                string newValue = value;
                string _tableName = "N0MI";
                int _idValue = -1; 
                int.TryParse(newValue, out _idValue);

                if (newValue.Length > 6)
                {
                    _tableName = "N" + newValue.Substring(0, newValue.Length - 6) + "MI";
                    _idValue = int.Parse(newValue.Substring(newValue.Length - 6, 6));
                }
                TableName = _tableName;
                _id = _idValue;

                newValue = null;
                _tableName = null;

            }
        }


        [JsonProperty(PropertyName = "P")]
        public bool IsPrime { get; set; }

        [JsonProperty(PropertyName = "L")]
        public string LowestFactorization { get; set; } = "";
    }


}