﻿using System;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using Nombro.Core;
using Nombro.Core.DAL;
using Nombro.Core.Workers;

namespace NombroCore
{
    class Program
    {
        static ThreadWorker threadWorker = ThreadWorker.Current;

        static NombroWorker nombroWorker;
        static string[] argsCache;

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            argsCache = args;
            DALDriver.Start();
            Thread.Sleep(200);
            LocalServer.Initialize();
            nombroWorker = new NombroWorker();
            Start();
            while (true)
            {
                Thread.Sleep(1000);
            }
        }

        private static void Start()
        {
            string[] args = argsCache;
            Console.Clear();
            if (args.Length == 0)
            {
                Console.Write(
                        "NombroCore <command> [options]" + Environment.NewLine +
                        " Commands are: " + Environment.NewLine +
                        "   primes - start prime classification" + Environment.NewLine +
                        "   sieve - start sieveing numbers" + Environment.NewLine +
                        "   sieveall - start sieveing all tables" + Environment.NewLine +
                        "   missingprimes - check for missing primes in" + Environment.NewLine +
                      
                        " Options are:" + Environment.NewLine +
                        "   primes [startpos]" + Environment.NewLine +
                        "   sieve <startpos> <endpos>" + Environment.NewLine +
                        "   sieveall - none" + Environment.NewLine +
                        "   missingprimes [startpos]" + Environment.NewLine 

                    );
            }
            else
            {


                string command = args[0];




                if (command == "sieve")
                {
                    string startPos = "0";
                    string endPos = "0";
                    if (args.Length > 2)
                    {
                        startPos = args[1];
                        endPos = args[2];
                        nombroWorker.StartSieve(startPos, endPos);
                    }
                    else
                    {
                        Console.WriteLine("Cowardly refusing to sieve without limit. :-)");
                    }


                }
                else
                if (command == "primes")
                {
                    string startPos = "0";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }

                    nombroWorker.StartPrimes(startPos);

                }
                else
                if (command == "missingprimes")
                {

                    string startPos = "0";
                    bool forceRun = false;
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }

                    if (args.Length > 2)
                    {
                        forceRun = true;
                    }

                    nombroWorker.StartMissingPrimes(startPos, forceRun);
                }
                else
                if (command == "convert")
                {
                    nombroWorker.ConvertToV2();
                }
                else
                if (command == "checkdouble")
                {
                    string startPos = "";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }
                    nombroWorker.CheckDouble(startPos);
                }
                else
                if (command == "sieveall")
                {
                    string startPos = "";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }
                    nombroWorker.SieveAll(startPos);
                }
                else
                if (command == "uncompress")
                {
                    string startPos = "";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }
                    nombroWorker.UncompressFile(startPos);
                }
                else
                if (command == "checktable")
                {
                    string startPos = "";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }
                    nombroWorker.CheckTable(startPos);
                }
                else
                if (command == "checkall")
                {
                    string startPos = "";
                    if (args.Length > 1)
                    {
                        startPos = args[1];
                    }
                    nombroWorker.CheckAll(startPos);
                }

                var lastMsg = DateTime.Now;
                while (LocalServer.GetCacheCount() > 0)
                {
                    if (lastMsg.AddSeconds(2) < DateTime.Now)
                    {
                        Console.WriteLine("Waiting for: " + DAL.Current.GetDALStats());
                        lastMsg = DateTime.Now;
                    }
                    Thread.Sleep(500);
                }
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ConsoleWorker.Exception(((Exception)e.ExceptionObject).ToString());
        }
    }
}
