﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using MathNet.Numerics;
using MoreLinq;
using Nombro.Core.DAL;
using Nombro.Core.Workers;
using Nombro.Objects.V2;

namespace Nombro.Core
{
    public partial class NombroWorker
    {
        public static DAL.DAL DAL = Core.DAL.DAL.Current;

        private int PrimeCount = 0;
        private int FacCount = 0;
        public MachineStats MachineStats { get; private set; }
        public bool IsRunning { get; set; }

        private int[] primeArray = new int[4] { 1, 3, 7, 9 };
        HashSet<BigInteger> existingNumbers;
        string lastTableName = "";

        private DateTime lastUpdate;


        public NombroWorker()
        {

            ConsoleWorker.Register(1);

            MachineStats = new MachineStats();

        }

        ~NombroWorker()
        {
        }



        public void StartPrimes(string startPosString = "0", bool startMissing = false)
        {
            if (startMissing)
                MachineStats.MachineType = "worker-missing";
            else
                MachineStats.MachineType = "worker";
            MachineStats.MachineName = "worker";

            BigInteger startPos = 0;
            BigInteger.TryParse(startPosString, out startPos);


            for (var i = 0; i < 1000000; i++)
            {
                RunPrimes(startPos + (i * 1000000), startMissing);
                GC.Collect(0, GCCollectionMode.Forced, true, true);
                GC.WaitForPendingFinalizers();
                System.Threading.Thread.Sleep(10);
            }
        }



        public void ConvertToV2(string startPosString = "")
        {
            var tables = GetTables();

            HashSet<string> done = new HashSet<string>();
            bool foundStart = startPosString == "" ? true : false;
            ConsoleWorker.Log("Starting at:" + startPosString);

            List<Task> tasks = new List<Task>();

            foreach (var table in tables.ToList())
            {
                if (!foundStart && startPosString != "" && table.tableInfo.Table.ToLower().Contains(startPosString))
                {
                    foundStart = true;
                }

                if (foundStart)
                {

                    if (!done.Contains(table.tableInfo.Table))
                    {

                        done.Add(table.tableInfo.Table);
                        string newTableName = table.tableInfo.Table != "NM" ? table.tableInfo.Table + "I" : "N0MI";

                        string fileName = LocalServer.DataDir + "/" + string.Join("/", newTableName.Replace("N", "").Replace("MI", "").ToCharArray()) + "/" + newTableName + ".json.lz4";

                        if (!File.Exists(fileName))
                        {

                            var task = Task.Run(() =>
                                {

                                    ConsoleWorker.Log("Converting " + table.tableInfo.Table);

                                    BigInteger tableStart = table.number * 1000000;
                                    var numbers = DAL.GetExistingNumbers(tableStart, tableStart + 999999);



                                    ConsoleWorker.Log("Converting " + table.tableInfo.Table + " - amount: " + numbers.Count + " to file:" + newTableName + ".json");


                                    int i = 0;
                                    var numbersV2 = numbers
                                        .Select(n =>
                                        {
                                            i++;

                                            var number = new Objects.V2.Number();

                                            number.Integer = n.Integer;
                                            number.IsPrime = n.IsPrime;
                                            number.LowestFactorization = n.LowestFactorization != null ? n.LowestFactorization : "";

                                            return number;
                                        }).ToList();

                                    ConsoleWorker.Log("Inserting to " + newTableName + "...");

                                    LocalServer.Insert(numbersV2);
                                    //LocalServer.Unload();


                                    ConsoleWorker.Log("Done inserting " + newTableName + ".");

                                });

                            tasks.Add(task);
                        }

                        while (tasks.Count > 8)
                        {
                            tasks.RemoveAll(t => t.IsCompleted);
                            Thread.Sleep(20);

                        }

                    }
                }
            }


        }

        public void UncompressFile(string startPosString = "")
        {

            string tableName = DAL.GetTableNameByIdentifier(startPosString);

            ConsoleWorker.Log("Uncompressing: " + tableName);

            LocalServer.ExportTable(tableName, "./" + tableName + ".json");

        }

        public void CheckAll(string startPosString)
        {
            BigInteger startPos = BigInteger.Parse(startPosString);
            ConsoleWorker.Log("Checking all tables.");
            ConsoleWorker.Log("Starting at: " + (startPos * 1000000));
            List<Task> tasks = new List<Task>();

            for (BigInteger pos = startPos; pos < 100000; pos++)
            {
                while (tasks.Count > 6)
                {
                    GC.Collect();
                    tasks.RemoveAll(t => t.IsCompleted || t.IsFaulted || t.IsCanceled);
                    Thread.Sleep(300);
                }


                BigInteger myPos = BigInteger.Parse(pos.ToString());
                string curPosString = (myPos * 1000000).ToString();

                try
                {
                    var task = Task.Run(() =>
                    {
                        Thread.Sleep(100);
                        CheckTable(curPosString);

                    });
                    tasks.Add(task);
                }
                catch (Exception ex)
                {

                    string tableName = DAL.GetTableNameByIdentifier(startPosString);
                    LocalServer.DeleteTable(tableName);
                    ConsoleWorker.Log("Deleted corrupted table: " + tableName);
                    ConsoleWorker.Exception(ex.ToString());
                }
                LocalServer.Unload();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        public void CheckTable(string startPosString)
        {
            string tableName = DAL.GetTableNameByIdentifier(startPosString);

            ConsoleWorker.Log("Checking table: " + tableName);

            BigInteger startPos = BigInteger.Parse(startPosString);
            BigInteger endPos = startPos + 999999;


            List<Number> numbers = LocalServer.GetTable(tableName);



            if (numbers.Count < 1000000)
            {
                HashSet<string> numberCache = MoreLinq.Extensions.ToHashSetExtension.ToHashSet(numbers.AsParallel().Select(n => n.Integer));

                lastUpdate = DateTime.Now;

                for (BigInteger pos = startPos; pos <= endPos; pos++)
                {
                    if (!numberCache.Contains(pos.ToString()))
                    {


                        if (lastUpdate.AddSeconds(1) < DateTime.Now)
                        {
                            ConsoleWorker.Log("Missing: " + pos.ToString());

                            lastUpdate = DateTime.Now;
                        }


                        bool baseIsPrime = isPrime(pos);

                        Number number = new Number();

                        number.Integer = pos.ToString();
                        number.IsPrime = baseIsPrime;
                        number.LowestFactorization = "";
                        PrimeCount++;
                        Task.Run(() =>
                        {
                            DAL.WriteNumber(number);
                        });

                    }
                }
            }

        }

        public void CheckDouble(string startPosString = "")
        {
            var tables = GetTables();

            HashSet<string> done = new HashSet<string>();
            bool foundStart = startPosString == "" ? true : false;
            ConsoleWorker.Log("Starting at:" + startPosString);
            int tcount = 0;

            foreach (var table in tables.ToList())
            {
                if (!foundStart && startPosString != "" && table.tableInfo.Table.ToLower().Contains(startPosString))
                {
                    foundStart = true;
                }

                if (foundStart)
                {

                    if (!done.Contains(table.tableInfo.Table))
                    {
                        done.Add(table.tableInfo.Table);
                        string numberString = table.number.ToString();
                        tcount++;
                        ConsoleWorker.Log("Checking N:" + numberString + " in table " + table.tableInfo.Table + " - " + tcount + "/" + tables.Count);

                        List<Number> toDelete = CheckDoubleinTable(table);

                        lastUpdate = DateTime.Now;

                        LocalServer.Delete(toDelete);
                    }
                }
            }
        }

        private static List<Number> CheckDoubleinTable(TableAndName table)
        {
            table.number = table.number * 1000000;
            string numberString = table.number.ToString();

            List<Number> existing = DAL.GetExistingNumbers(table.number, table.number + 999999);
            ConsoleWorker.Log("Table has " + existing.Count + " numbers");
            HashSet<string> existingIntegers = new HashSet<string>();
            HashSet<string> doubleIntegers = new HashSet<string>();
            List<Number> toDelete = new List<Number>();
            foreach (var number in existing)
            {
                if (existingIntegers.Contains(number.Integer) && !doubleIntegers.Contains(number.Integer))
                {
                    doubleIntegers.Add(number.Integer);
                    toDelete.Add(number);
                }
                else if (!existingIntegers.Contains(number.Integer))
                {
                    existingIntegers.Add(number.Integer);
                }
            }

            ConsoleWorker.Log("Found " + doubleIntegers.Count + " doublettes");
            return toDelete;
        }

        private List<TableAndName> GetTables()
        {
            List<TableAndName> list = LocalServer.GetTables();
            return list;
        }

        public void SieveAll(string startPosString = "")
        {

            ConsoleWorker.Log("Starting at:" + startPosString);
            BigInteger initPos = BigInteger.Parse(startPosString);
            for (BigInteger pos = initPos; pos < initPos + 100000; pos++)
            {
                BigInteger startPos = (pos * 1000000);
                BigInteger endPos = (pos * 1000000) + 999999 + 1;
                ConsoleWorker.Log("______________________________________________");
                ConsoleWorker.Log(DateTime.Now.ToString());
                ConsoleWorker.Log("From: " + startPos);
                ConsoleWorker.Log("To: " + endPos);
                StartSieve(startPos.ToString(), endPos.ToString());
                GC.Collect();
            }


        }

        private bool forceRun;

        public void StartSieve(string startPosString, string endPosString)
        {
            HashSet<string> sieveCache = new HashSet<string>();

            MachineStats.MachineType = "siever";
            MachineStats.MachineName = "worker";

            BigInteger startPos = 0;
            BigInteger.TryParse(startPosString, out startPos);
            BigInteger endPos = 0;
            BigInteger.TryParse(endPosString, out endPos);

            BigInteger primeStart = 1;
            BigInteger primeEnd = Sqrt(endPos + 2) * 2;



            int dCount = DAL.CountExistingNumbers(startPos, endPos);
            if (dCount > 999999 || (dCount > 999998 && startPos < 2))
            {
                ConsoleWorker.Log("Table is full.");
                return;

            }

            List<Number> existing = DAL.GetExistingNumbers(startPos, endPos);
            AddSieveCache(existing, sieveCache);

            List<Number> basePrimes = DAL.GetExistingNumbers(primeStart, primeEnd, true);



            AddMissingBasePrimes(ref existing, ref basePrimes);
            ConsoleWorker.Log("Existing: " + existing.Count);

            basePrimes = GetUsablePrimes(primeEnd, basePrimes);

            ConsoleWorker.Log("Amount of usable primes:" + basePrimes.Count + " from:" + basePrimes.First().Integer + " to:" + basePrimes.Last().Integer);
            AddSieveCache(basePrimes, sieveCache);
            List<Task> tasks = new List<Task>();

            foreach (var primeNumber in basePrimes)
            {

                BigInteger prime = BigInteger.Parse(primeNumber.Integer);

                BigInteger chainStart = startPos - prime;
                while (chainStart % prime != 0)
                {
                    chainStart++;
                }

                var numberPos = chainStart;
                while (numberPos <= endPos)
                {
                    string numberString = numberPos.ToString();
                    string tableName = DAL.GetTableNameByIdentifier(numberString);

                    var task = Task.Run(() =>
                     {
                         if (!sieveCache.Contains(numberString) && numberPos >= startPos && numberPos <= endPos)
                         {
                             Number number = new Number()
                             {

                             };

                             number.Integer = numberString;
                             number.IsPrime = false;
                             number.LowestFactorization = prime.ToString();
                             DAL.WriteNumber(number);

                             sieveCache.Add(numberString);
                         }
                     });

                    tasks.Add(task);

                    if (lastUpdate < DateTime.Now.AddSeconds(-1))
                    {
                        ConsoleWorker.Log("Running prime: " + prime.ToString() + " - Starting at " + chainStart);
                        ConsoleWorker.Log("\r" + tableName + "P:" + prime.ToString() + " - F:" + numberString + " - Fac/Sec:" + FacCount + "                        ");
                        lastUpdate = DateTime.Now;
                        FacCount = 0;
                    }

                    numberPos = numberPos + prime;
                    FacCount++;
                }


            }


            if (LocalServer.GetCacheCount() > 0 || tasks.Count > 0)
            {
                tasks.RemoveAll(t => t.IsCompleted || t.IsFaulted || t.IsCanceled);
                ConsoleWorker.Log("Waiting for cache to write: " + LocalServer.GetCacheCount());

                Thread.Sleep(100);
            }

            GC.Collect();
        }


        private static List<Number> GetUsablePrimes(BigInteger primeEnd, List<Number> basePrimes)
        {
            basePrimes = basePrimes
                .Where(p =>
                {
                    var bi = BigInteger.Parse(p.Integer);
                    return bi <= primeEnd && p.IsPrime;
                })
                .OrderBy(p => BigInteger.Parse(p.Integer)).ToList();
            return basePrimes;
        }

        private static void AddMissingBasePrimes(ref List<Number> existing, ref List<Number> basePrimes)
        {
            var checkBase10Primes = new List<string>() { "2", "3", "5", "7", "11", "13", "17", "19" };
            existing = existing.Where(e => !checkBase10Primes.Contains(e.Integer)).ToList();
            basePrimes = basePrimes.Where(e => !checkBase10Primes.Contains(e.Integer)).ToList();
            ConsoleWorker.Log("Dirty adding prime: ");
            foreach (var checkPrime in checkBase10Primes)
            {
                //dirty hack because optimized isprime wont deliver these
                string checkString = Convert.ToString(checkPrime);
                ConsoleWorker.Log(checkString + ",");

                basePrimes.Add(new Number()
                {
                    Integer = checkString,
                    IsPrime = true,
                    LowestFactorization = "",
                });

            }
        }

        private void AddSieveCache(List<Number> existing, HashSet<string> sieveCache)
        {
            existing.ForEach(n => sieveCache.Add(n.Integer));
        }





        public void StartMissingPrimes(string startPosString = "0", bool _forceRun = false)
        {
            forceRun = _forceRun;
            StartPrimes(startPosString, true);
        }

        private void RunPrimes(BigInteger startPos, bool searchMissing)
        {

            IsRunning = true;
            int amount = 1000000;

            string startString = startPos.ToString();

            ConsoleWorker.Log("Searching primes: " + startString);
            string tableName = DAL.GetTableNameByIdentifier(startString);




            ConsoleWorker.Log("Table name: " + tableName);



            if (tableName != lastTableName)
            {
                var existing = DAL.GetExistingNumbers(startPos, startPos + amount);
                existingNumbers = new HashSet<BigInteger>(existing.Select(n => BigInteger.Parse(n.Integer)));
                lastTableName = tableName;
            }

            ConsoleWorker.Log(tableName + " has " + existingNumbers.Count + " numbers.");
            HashSet<BigInteger> missingNumbers = GenerateNumbers(startPos, amount, primeArray);
            ConsoleWorker.Log(tableName + " may have " + missingNumbers.Count + " missing.");


            List<Task> tasks = new List<Task>();

            foreach (var number in missingNumbers)
            {
                while (tasks.Count > 299)
                {
                    tasks.RemoveAll(t => t.IsCanceled || t.IsCompleted || t.IsFaulted);
                    Thread.Sleep(1);
                }

                if (tasks.Count < 300)
                    tasks.Add(Task.Run(() => SavePrime(number)));




                if (lastUpdate < DateTime.Now.AddSeconds(-1))
                {
                    ConsoleWorker.Log("\r" + number.ToString() + " - P/Sec:" + PrimeCount + "                        ");
                    lastUpdate = DateTime.Now;
                    PrimeCount = 0;
                }
            }

            GC.Collect();

            if (LocalServer.GetCacheCount() > 0)
            {
                Thread.Sleep(100);
            }



        }

        private bool SavePrime(BigInteger inputNumber)
        {
            bool baseIsPrime = isPrime(inputNumber);
            if (baseIsPrime)
            {
                Number number = new Number();

                number.Integer = inputNumber.ToString();
                number.IsPrime = baseIsPrime;
                number.LowestFactorization = "";
                PrimeCount++;

                Task.Run(() => DAL.WriteNumber(number));

            }
            return baseIsPrime;
        }

        bool isPrime(BigInteger number)
        {

            if (number == 1) return false;
            if (number == 2) return true;

            string pString = number.ToString();
            char lastDigit = pString[pString.Length - 1];
            if (
            lastDigit == '0' ||
            lastDigit == '2' ||
            lastDigit == '4' ||
            lastDigit == '5' ||
            lastDigit == '6' ||
            lastDigit == '8'
            )
                return false;

            if (isDivisibleBy7(number))
                return false;


            BigInteger limit = Sqrt(number);

            for (BigInteger i = 3; i <= limit; i = i + 2)
            {
                if (MathNet.Numerics.Euclid.Modulus(number, i) == 0) return false;
            }


            return true;

        }

        bool isDivisibleBy7(BigInteger num)
        {

            // If number is negative,  
            // make it positive 
            if (num < 0)
                return isDivisibleBy7(-num);

            // Base cases 
            if (num == 0 || num == 7)
                return true;
            if (num < 10)
                return false;

            // Recur for ( num / 10 - 2 * num % 10 )  
            return isDivisibleBy7(num / 10 - 2 *
                                 (num - num / 10 * 10));
        }


        public BigInteger Sqrt(BigInteger n)
        {
            Complex complex = (Complex)n;
            complex = ComplexModule.sqrt(complex);

            return new BigInteger(complex.Real);
        }







        private HashSet<BigInteger> GenerateNumbers(BigInteger startPos, int amount, int[] baseNumbers)
        {
            HashSet<BigInteger> newnumbers = new HashSet<BigInteger>();
            BigInteger end = (startPos / 10) + (amount / 10);
            BigInteger start = startPos / 10;

            for (var i = start; i < end; i++)
            {
                foreach (var bNumber in baseNumbers)
                {
                    BigInteger number = i * 10 + bNumber;
                    if (!existingNumbers.Contains(number))
                    {
                        newnumbers.Add(number);
                    }
                }
            }

            return newnumbers;
        }
    }
}
