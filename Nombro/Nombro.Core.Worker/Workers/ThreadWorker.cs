﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nombro.Core.Workers
{


    public class ManagedThread
    {
        private Thread thread;
        private ThreadPriority priority;
        private ThreadStart startMethod;
        public bool IsRunning;
        public bool IsFailed;

        public ManagedThread(ThreadStart _startMethod, ThreadPriority _priority = ThreadPriority.Normal, ThreadState startState = ThreadState.Running)
        {
            startMethod = _startMethod;
            priority = _priority;

            thread = new Thread(ExecuteStartMethod)
            {
                Priority = priority,
                IsBackground = true
            };

            if (startState == ThreadState.Running)
                thread.Start();
        }

        public void Start()
        {
            thread.Start();
        }

        private void ExecuteStartMethod()
        {
            try
            {
                IsRunning = true;
                IsFailed = false;
                startMethod();
            }
            catch (Exception ex)
            {
                IsFailed = true;
                IsRunning = false;
                ConsoleWorker.Exception(ex.ToString());

            }
            finally
            {
                IsRunning = false;
            }
        }

        public void DisposeThread()
        {
            if (thread.IsAlive)
                thread.Abort();
            thread = null;
            startMethod = null;
        }
    }

    public class ThreadWorker
    {
        private static ThreadWorker _current;
        public static ThreadWorker Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new ThreadWorker();
                }
                return _current;
            }
        }

        private Timer timer;
        private DateTime lastCheck = DateTime.Now;
        private int timerUpdateTime = 3000;
        private readonly bool IsRunning;

        private readonly ConcurrentDictionary<Guid, ManagedThread> threads = new ConcurrentDictionary<Guid, ManagedThread>();
        private readonly object threadArrayLock = new object();

        public ThreadWorker()
        {
            ConsoleWorker.Register(2);
            ConsoleWorker.Log("starting");
            ThreadPool.SetMaxThreads(512, 1024);

            timer = new Timer(OnTimerTick, null, timerUpdateTime, Timeout.Infinite);

            ConsoleWorker.Log("started");
        }

        private void OnTimerTick(object state)
        {
            UpdateTasks();
            timer.Change(timerUpdateTime, Timeout.Infinite);
        }

        ~ThreadWorker()
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
            WaitForAll();
        }

        private void WaitForAll()
        {
            while (threads.Count > 0)
            {
                ConsoleWorker.Log("Waiting for " + threads.Count + " threads.");
                ClearThreads();
                Thread.Sleep(1000);
            }
        }

        private void ClearThreads()
        {
            foreach (var thread in threads.ToList())
            {
                if (!thread.Value.IsRunning)
                {
                    ManagedThread managedThread;
                    while (!threads.TryRemove(thread.Key, out managedThread))
                    {
                        Thread.Sleep(2);
                    }
                    // managedThread.DisposeThread();
                }
            }
        }

        public ManagedThread AddThread(ThreadStart threadStart, ThreadPriority priority = ThreadPriority.Normal, ThreadState startState = ThreadState.Running)
        {

            //ConsoleWorker.Log("Added thread");

            ManagedThread thread = new ManagedThread(threadStart, priority, startState);
            threads.GetOrAdd(Guid.NewGuid(), thread);

            return thread;
        }

        public int GetThreadCount()
        {
            ClearThreads();
            return threads.Count;
        }

        private void UpdateTasks()
        {
            ClearThreads();
            if (DateTime.Now.Second % 3 == 0)
                ConsoleWorker.Log("Threads: " + threads.Count);
        }

        public void WaitAll()
        {
            WaitForAll();
        }
    }
}
