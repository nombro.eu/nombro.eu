﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Nombro.Objects;
using NPoco;

namespace Nombro
{
    public class PerformanceCounterService
    {
        public MachineStats MachineStats { get; set; }
        PerformanceCounter cpuCounter;
        PerformanceCounter ramCounter;

        public void Start()
        {
            cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");

            for (; ; )
            {
                if (MachineStats.MachineName != "" && MachineStats.MachineName != null)
                    GetMachineInfos();


                if (MachineStats.MachineName == "exit") return;

                Thread.Sleep(1000);
            }
        }

        string getCurrentCpuUsage()
        {
            return cpuCounter.NextValue() + "%";
        }

        string getAvailableRAM()
        {
            return ramCounter.NextValue() + "MB";
        }

        private void GetMachineInfos()
        {
            try
            {

                MachineStats.FreeRam = getAvailableRAM();
                MachineStats.UsedCpu = getCurrentCpuUsage();



                /*                string createSQL = @"CREATE TABLE IF NOT EXISTS `AMachineStats` (
                                `Id` INT(11) NOT NULL AUTO_INCREMENT,
                                `MachineType` VARCHAR(100) NOT NULL,
                                `MachineName` VARCHAR(100) NOT NULL,
                                `FreeRam` VARCHAR(100) NOT NULL,
                                `UsedCpu` VARCHAR(100) NOT NULL,
                                `LastWrite` DATETIME NOT NULL,
                                PRIMARY KEY (`Id`)
                            );";
                            */
                var db = new Database("server=currentdb;database=Numbers;user id=Numbers;password=1100101;port=3306",
                              DatabaseType.MySQL,
                              MySql.Data.MySqlClient.MySqlClientFactory.Instance);

                using (var transaction = db.GetTransaction(IsolationLevel.ReadUncommitted))
                {
                    db.EnableAutoSelect = false;

                    //      db.Execute(createSQL);

                    MachineStats machineStats = db.FirstOrDefault<MachineStats>("select * from AMachineStats where MachineName='" + MachineStats.MachineName + "'");
                    if (machineStats == null)
                        machineStats = new MachineStats();

                    machineStats.MachineName = MachineStats.MachineName;
                    machineStats.MachineType = MachineStats.MachineType;
                    machineStats.UsedCpu = MachineStats.UsedCpu;
                    machineStats.FreeRam = MachineStats.FreeRam;
                    machineStats.LastWrite = DateTime.Now;

                    if (machineStats.Id < 1)
                    {
                        db.Insert("AMachineStats", "Id", machineStats);
                    }
                    else
                    {
                        db.Update("AMachineStats", "Id", machineStats);
                    }

                    db.EnableAutoSelect = true;

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}