﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace Nombro.Core.Workers
{
    public static class ConsoleWorker
    {
        static ConcurrentDictionary<string, int> caller = new ConcurrentDictionary<string, int>();
        static List<string> rotateLog = new List<string>();
        static object rotateLock = new object();

        public static void Register(int rowNumber, [CallerFilePath] string memberFile = "", [CallerMemberName] string memberName = "")
        {
            memberFile = GetMemberFile(memberFile);
            caller.GetOrAdd(memberFile, rowNumber);
        }

        private static string GetMemberFile(string memberFile)
        {
            return Path.GetFileNameWithoutExtension(memberFile);

        }

        public static void Exception(string outputString, [CallerFilePath] string memberFile = "", [CallerMemberName] string memberName = "")
        {
            int row = 12;

            Console.SetCursorPosition(0, row);
            Log(outputString, memberFile, memberName);

            Console.Write(new String(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, row);
            Console.Write(memberFile + " + " + memberName + " - Exception: " + outputString);
        }

        static int tId = Thread.CurrentThread.ManagedThreadId;
        private static object lockScreenObject = new object();

        public static void Log(string outputString, [CallerFilePath] string memberFile = "", [CallerMemberName] string memberName = "")
        {
            lock (lockScreenObject)
            {

                try
                {
                    memberFile = GetMemberFile(memberFile);
                    int row = 10;
                    caller.TryGetValue(memberFile, out row);

                    Console.SetCursorPosition(0, row);

                    Console.Write(new String(' ', Console.WindowWidth));
                    Console.SetCursorPosition(0, row);

                    Console.Write(" >" + memberFile + " - " + memberName + " - " + outputString);
                    Rotate(outputString, memberFile, memberName);

                }
                catch
                {

                    return;
                }
            }
        }

        public static void Rotate(string outputString, [CallerFilePath] string memberFile = "", [CallerMemberName] string memberName = "")
        {
            try
            {
                lock (rotateLock)
                {
                    rotateLog.Add(memberFile + " - " + memberName + " - " + outputString);
                    rotateLog = rotateLog.Skip(rotateLog.Count - (Console.WindowHeight - 16)).Take(Console.WindowHeight - 16).ToList();

                    int row = 13;
                    foreach (var line in rotateLog)
                    {
                        Console.SetCursorPosition(0, row);

                        Console.Write(new String(' ', Console.WindowWidth - line.Length));

                        Console.SetCursorPosition(0, row);

                        Console.Write(" |" + line);

                        row++;
                    }
                }
            }
            catch
            {
                return;
            }
        }
    }
}
