﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nombro.Core.Workers;
using Nombro.Objects.V2;

namespace Nombro.Core.DAL
{
    public static class MQRegistry
    {
        static MQRegistry()
        {
            ConsoleWorker.Register(9);
            ConsoleWorker.Log("Registered MQRegistry.");
        }

        private static ConcurrentDictionary<string, object> currentQueues = new ConcurrentDictionary<string, object>();

        public static MQQueue<T> RegisterQueue<T>(Action<T> dequeueAction)
        {
            string queueName = dequeueAction.Method.DeclaringType.Name + "." + dequeueAction.Method.Name;

            MQQueue<T> queue = new MQQueue<T>(queueName);
            queue.DequeueAction = dequeueAction;

            if (!currentQueues.TryAdd(queueName, queue))
            {
                ConsoleWorker.Log("Could not add MQQueue:" + queueName);
            }
            else
            {
                ConsoleWorker.Log("Added MQServer: " + queueName);
            }


            return queue;
        }

        public static MQQueue<T> GetQueue<T>(string queueName)
        {
            if (currentQueues.TryGetValue(queueName, out object result))
            {
                ConsoleWorker.Log("Offering queue: " + queueName);
                MQQueue<T> queue = (MQQueue<T>)result;
                return queue;
            }
            else
            {
                ConsoleWorker.Log("Could not find queue: " + queueName);
            }

            return default;
        }
    }

    public class MQQueue<T>
    {
        public Action<T> DequeueAction { get; internal set; }
        public ConcurrentQueue<T> queue = new ConcurrentQueue<T>();
        private object asyncTask;
        private bool IsRunning;
        private ManagedThread thread;

        public MQQueue(string queueName)
        {
            thread = ThreadWorker.Current.AddThread(CheckQueue, System.Threading.ThreadPriority.AboveNormal, System.Threading.ThreadState.Running);

        }

        void CheckQueue()
        {
            IsRunning = true;
            while (IsRunning)
            {
                while (queue.Count == 0) Thread.Sleep(10);

                while (queue.TryDequeue(out T result))
                {
                    DequeueAction.Invoke(result);
                }
            }
        }

        public void Enqueue(T queueObject)
        {
            queue.Enqueue(queueObject);
        }
    }

}
