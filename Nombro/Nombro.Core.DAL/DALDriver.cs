﻿using System;
using System.Threading.Tasks;
using Nombro.Core.Workers;

namespace Nombro.Core.DAL
{
    public static class DALDriver
    {

        static ThreadWorker threadWorker = ThreadWorker.Current;
        public static bool IsRunning;
        public static bool IsStarted;
        public static void Start()
        {
            if (!IsRunning)
            {
                IsRunning = true;
                Initialize();
//                threadWorker.AddThread(Initialize, System.Threading.ThreadPriority.Normal);
            }
        }

        public static void Initialize()
        {
            if (!IsStarted)
            {
                IsStarted = true;
                DAL.Current = new DAL();

            }
        }

        public static bool IsStarting()
        {
            if(!IsStarted)
            {
                Start();
            }

            return !IsRunning;
        }
    }
}
