﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using MoreLinq;
using Nombro.Core.Workers;
using Nombro.Objects.V2;
using NPoco;
using TableInfo = Nombro.Objects.TableInfo;

namespace Nombro.Core.DAL
{
    public class DAL
    {
        private static DAL _current;
        private object LockObject = new object();
        private ConcurrentQueue<Number> writeCache = new ConcurrentQueue<Number>();
        private bool isWriting = false;
        private DateTime lastAdd = DateTime.Now;

        static ThreadWorker threadWorker = ThreadWorker.Current;
        private bool isRunning;
        private MQQueue<Number> serverQueue;

        public static DAL Current
        {
            get
            {
                return _current;
            }

            set
            {
                _current = value;
            }
        }


        public DAL()
        {

            ConsoleWorker.Register(4);
            ConsoleWorker.Log("Starting DAL");
            ConsoleWorker.Log("Started DAL");
            Current = this;
            isRunning = true;

            Task.Run(() =>
            {
                while (serverQueue == null)
                {
                    serverQueue = MQRegistry.GetQueue<Number>("LocalServer.Insert");
                    Thread.Sleep(100);
                }
            });

          //  watchThread = ThreadWorker.Current.AddThread(WatchQueue, ThreadPriority.Highest);
        }

        public void WriteNumber(Number number)
        {
            serverQueue.Enqueue(number);
        }




        public void DeleteNumber(Number number)
        {
            LocalServer.Delete(new List<Number>() { number });
        }



        public string GetDALStats()
        {
            return "Numbers: " + LocalServer.GetCacheCount() + " Threads:" + threadWorker.GetThreadCount();
        }




        public void DebugObject(object obj)
        {
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(obj);
                string output = "";
                output = output + String.Format("{0}={1}", name, value) + "\n";
                ConsoleWorker.Log(output);
            }
        }

        public int CountExistingNumbers(BigInteger from, BigInteger to)
        {
            string tableName = GetTableNameByIdentifier(from.ToString());

            int countNumbers = LocalServer.GetTable(tableName).Count;

            return countNumbers;
        }

        public List<Number> GetExistingNumbers(BigInteger from, BigInteger to, bool v = false)
        {
            List<Number> outputList = new List<Number>();

            while (from < to)
            {
                string tableName = GetTableNameByIdentifier(from.ToString());

                List<Number> currentNumbers = LocalServer.GetTable(tableName, v).ToList();
                outputList.AddRange(currentNumbers);
                from = from + 1000000;
            }

            return outputList;
        }


        public string GetTableNameByIdentifier(string identifier)
        {

            int IdentifierSplitLength = 6;
            identifier = "         " + identifier;

            string fName = identifier.Substring(0, identifier.Length - IdentifierSplitLength);
            fName = fName.Trim();
            if (fName == "") fName = "0";
            fName = "N" + fName + "MI";

            return fName;
        }

    }
}
