﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using MoreLinq;
using Nombro.Core.Workers;
using Nombro.Objects.V2;

namespace Nombro.Core.DAL
{
    public static class LocalServer
    {
        public static string DataDir { get; private set; }
        public static bool IsRunning { get; private set; }


        static object LockObject = new object();
        static ThreadWorker threadWorker = ThreadWorker.Current;
        static ConcurrentDictionary<string, LocalFile> files = new ConcurrentDictionary<string, LocalFile>();
        static ConcurrentQueue<Number> writeCache = new ConcurrentQueue<Number>();
        static ConcurrentQueue<Number> deleteCache = new ConcurrentQueue<Number>();
        static MQQueue<Number> queue;
        private static DateTime lastWrite;

        static LocalServer()
        {
            ConsoleWorker.Register(8);
            ConsoleWorker.Log("Registered LocalServer.");
        }

        public static void Initialize(string dataDir = "/data/files/")
        {
            queue = MQRegistry.RegisterQueue<Number>(Insert);


            DataDir = dataDir;

            while (threadWorker == null)
            {
                ConsoleWorker.Log("Waiting for threadWorker");
                Thread.Sleep(1000);
            }

            IsRunning = true;
            threadWorker.AddThread(MainLoop, ThreadPriority.Highest);
        }

        public static void ReceiveHandler(List<Number> receiveNumbers)
        {

        }

        public static int GetCacheCount()
        {
            return writeCache.Count + deleteCache.Count;
        }

        public static List<Number> GetTable(string Tablename, bool v = false)
        {
            List<Number> numbers = new List<Number>();

            LocalFile localFile = GetLocalFile(Tablename);
            localFile.KeepRunning = v;
            while (!localFile.IsRunning) Thread.Sleep(10);

            numbers = localFile.GetTable();

            return numbers;
        }

        private static LocalFile GetLocalFile(string Tablename)
        {
            LocalFile localFile;

            if (!files.TryGetValue(Tablename, out localFile))
            {
                Task.Run(() =>
                {
                    localFile = new LocalFile(Tablename);
                    files.TryAdd(Tablename, localFile);
                }).GetAwaiter().GetResult();
            }

            return localFile;

        }


        public static void Insert(Number number)
        {
            writeCache.Enqueue(number);
        }

        public static void Insert(List<Number> newNumbers)
        {
            Parallel.ForEach(newNumbers, (newNumber) =>
                    writeCache.Enqueue(newNumber));


        }

        public static void Delete(List<Number> delNumbers)
        {

            foreach (var delNumber in delNumbers)
            {

                Task.Run(() =>
                {
                    deleteCache.Enqueue(delNumber);
                });
            }
        }

        private static void MainLoop()
        {
            DateTime lastDisplayUpdate = DateTime.Now;
            ConsoleWorker.Log("Started LocalServer MainLoop");
            IsRunning = true;
            while (IsRunning)
            {

                while ((writeCache.Count < 50000) && lastWrite.AddSeconds(3) > DateTime.Now)
                {
                    Thread.Sleep(300);
                }

                if (writeCache.Count > 0)
                {
                    lastWrite = DateTime.Now;
                    ConsoleWorker.Log("Writing cache to files - amount: " + writeCache.Count);
                    List<Number> numbers = new List<Number>();
                    Number outNumber;
                    while (writeCache.TryDequeue(out outNumber))
                    {
                        numbers.Add(outNumber);
                    }

                    foreach (var numberGroup in numbers.GroupBy(n => n.TableName))
                    {
                        ConsoleWorker.Log("Writing to table: " + numberGroup.Key + " - amount: " + numberGroup.Count());
                        var localFile = GetLocalFile(numberGroup.First().TableName);
                        localFile.AddNumbers(numberGroup.ToList());
                    }
                }



                if (lastDisplayUpdate < DateTime.Now.AddSeconds(-3))
                {
                    ConsoleWorker.Log("Server active. Files:" + files.Count + " Cache: " + writeCache.Count);
                    lastDisplayUpdate = DateTime.Now;
                    foreach (var file in files.ToList())
                    {
                        if (file.Value.LastUsage < DateTime.Now.AddSeconds(-30) && !file.Value.IsWriting && !file.Value.IsBusy && !file.Value.KeepRunning)
                        {
                            ConsoleWorker.Log("Unloading file:" + file.Key);
                            Task.Run(() =>
                            UnloadFile(file.Value));
                        }
                        else
                        {
                            ConsoleWorker.Log("File: "
                                + file.Key + " Keep:" + file.Value.KeepRunning
                                + " LUse:" + file.Value.LastUsage
                                + " LWrite:" + file.Value.lastWrite
                                + " Writing:" + file.Value.IsWriting
                                + " Busy:" + file.Value.IsBusy);
                        }

                    }

                }

            }
        }

        public static void ExportTable(string tableName, string fileName)
        {
            LocalFile localFile = GetLocalFile(tableName);
            localFile.ExportFile(fileName, false);
        }

        public static void Unload()
        {
            foreach (var file in files)
            {
                UnloadFile(file.Value);
            }
        }

        private static void UnloadFile(LocalFile file)
        {
            LocalFile outFile;


            file.SaveFile();
            file.IsRunning = false;
            while (file.IsWriting)
            {
                Thread.Sleep(20);
            }

            ConsoleWorker.Log("Unloading file: " + file.Filename + " for " + file.Tablename + " - amount: " + file.NumbersCount);

            while (!files.TryRemove(GetFileNameWithoutExtension(file.Filename), out outFile)) { Thread.Sleep(1); }

            GC.Collect();
            ConsoleWorker.Log("Unloaded file: " + file.Filename + " for " + file.Tablename + " - amount: " + file.NumbersCount);

        }

        public static void DeleteTable(string tableName)
        {
            LocalFile localFile = GetLocalFile(tableName);

            Directory.Delete(localFile.Filename);
        }

        public static List<TableAndName> GetTables()
        {
            var fileNames = Directory.EnumerateFiles(DataDir, "*.json.lz4", SearchOption.AllDirectories);

            List<TableAndName> tableAndNames =
                fileNames
                    .AsParallel()
                    .Select(f =>
                    {
                        var tpos = GetFileNameWithoutExtension(f).Replace("N", "").Replace("MI", "");
                        BigInteger num = 0;

                        if (!BigInteger.TryParse(tpos, out num))
                        {
                            num = 0;
                        }

                        num = num * 1000000;

                        return
                        new TableAndName()
                        {

                            tableInfo = new Objects.TableInfo()
                            {
                                Table = GetFileNameWithoutExtension(f)
                            },
                            number = num,
                        };
                    }).ToList();

            tableAndNames = tableAndNames.OrderBy(tn => tn.number).ToList();

            return tableAndNames;
        }

        private static string GetFileNameWithoutExtension(string f)
        {
            return Path.GetFileNameWithoutExtension(f).Replace(".json", "");
        }
    }
}
