﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LZ4;
using K4os.Compression.LZ4;
using K4os.Compression.LZ4.Streams;
using MoreLinq;
using Newtonsoft.Json;
using Nombro.Core.Workers;
using Nombro.Objects.V2;
using LZ4Codec = LZ4.LZ4Codec;
using LZ4Stream = LZ4.LZ4Stream;
using System.Reflection;

namespace Nombro.Core.DAL
{
    public class LocalFile
    {
        public string DataDir;
        public bool IsRunning;
        public bool isChanged;
        public string Filename { get; private set; }
        public string Tablename { get; private set; }
        public bool KeepRunning;
        public Mutex FileLock;

        public ConcurrentDictionary<int, Number> numberCache = new ConcurrentDictionary<int, Number>();

        public bool IsWriting { get; internal set; }

        public bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }

            set
            {

                LastUsage = DateTime.Now;
                _IsBusy = value;
            }
        }


        public int NumbersCount
        {
            get
            {
                return numberCache.Count;
            }

        }

        public DateTime LastUsage { get; internal set; }

        private object lockObject = new object();
        private DateTime lastUpdate;
        public DateTime lastWrite;


        public LocalFile(string _Tablename, string _DataDir = "/data/files/")
        {
            IsBusy = true;

            ConsoleWorker.Register(9);
            Tablename = _Tablename;
            DataDir = _DataDir;

            Filename = GetFilenameForTable(Tablename);

            FileLock = new Mutex(false, Tablename);

            if (Filename == "")
            {
                Filename = DataDir + "/" + string.Join("/", Tablename.Replace("N", "").Replace("MI", "").ToCharArray()) + "/" + Tablename + ".json.lz4";
                Filename = Filename.Replace("//", "/");
                ConsoleWorker.Log("File not found - creating: " + Filename);
                SaveFile();

            }

            ConsoleWorker.Log("Opening " + Filename + " for table: " + Tablename);

            LoadFile();

            lastWrite = DateTime.Now;
            lastUpdate = lastWrite;

            IsBusy = true;
            ThreadWorker.Current.AddThread(HandleLocalFile, ThreadPriority.AboveNormal, ThreadState.Running);
        }

        private string GetFilenameForTable(string tablename)
        {

            List<string> fileNames = Directory.EnumerateFiles(DataDir, tablename + ".*", SearchOption.AllDirectories).ToList();

            if (fileNames.Count > 0) return fileNames.First();

            return "";
        }

        private string GetFileNameWithoutExtension(string f)
        {
            return Path.GetFileNameWithoutExtension(f).Replace(".json", "");
        }


        public void AddNumbers(List<Number> numbers)
        {
            IsBusy = true;
            isChanged = true;

            Parallel.ForEach(numbers, (number) =>
            {
                if (numberCache.ContainsKey(number.NId))
                {
                    numberCache[number.NId] = number;
                }
                else
                {
                    numberCache.GetOrAdd(number.NId, number);
                }
            });
            lastUpdate = DateTime.Now;

            IsBusy = false;
        }

        public void LoadFile()
        {

            try
            {
                IsBusy = true;

                FileLock.WaitOne();

                if (!Directory.Exists(Path.GetDirectoryName(Filename)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(Filename));
                }

                if (!File.Exists(Filename))
                {
                    File.Create(Filename);
                }

                bool doConvert = false;
                IsBusy = true;



                List<Number> loadedNumbers = new List<Number>();

                string json = "";



                try
                {
                    using (FileStream stream = new FileStream(Filename, FileMode.Open, FileAccess.Read, FileShare.None, 512 * 1024, true))
                    {


                        //           if (header[0] == 1 && header[1] == 128 && header[2] == 128)
                        //          {
                        try
                        {
                            using (var source = K4os.Compression.LZ4.Streams.LZ4Stream.Decode(stream, 4 * 1024 * 1024))
                            {
                                using (var reader = new StreamReader(source))
                                {
                                    //   json = reader.ReadToEndAsync().GetAwaiter().GetResult();

                                    using (JsonReader jsonReader = new JsonTextReader(reader))
                                    {
                                        JsonSerializer serializer = new JsonSerializer();
                                        loadedNumbers = serializer.Deserialize<List<Number>>(jsonReader);

                                        IsBusy = true;
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            File.Delete(Filename);
                            

                        }
                    }
                }
                catch (Exception ex)
                {
                    ex.SetCode(0);
                }



                if (loadedNumbers == null)
                    loadedNumbers = new List<Number>();


                Parallel.ForEach(loadedNumbers, (n) =>
                {
                    n.TableName = Tablename;
                }
                );


                numberCache = new ConcurrentDictionary<int, Number>(loadedNumbers.AsParallel().Select(n => new KeyValuePair<int, Number>(n.NId, n)));

                IsBusy = false;
                isChanged = false;

                FileLock.ReleaseMutex();

                if (doConvert)
                {
                    isChanged = true;
                    SaveFile();
                }

                GC.Collect();

            }
            catch (Exception ex)
            {
                ConsoleWorker.Log(ex.ToString());
                ConsoleWorker.Log("Corrupted file: " + Filename);
                //SaveFile();
            }
        }

        public List<Number> GetTable()
        {
            while (!IsRunning)
            {
                Thread.Sleep(10);
            }

            lock (lockObject)
            {
                IsBusy = true;
                List<Number> numbers = new List<Number>(numberCache.Count);
                numbers = numberCache.AsParallel().Select(kv => kv.Value).ToList();
                IsBusy = false;
                return numbers;
            }
        }

        private byte[] StringToByteArray(string str)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetBytes(str);
        }

        private string ByteArrayToString(byte[] arr)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetString(arr);
        }

        public void ExportFile(string fileName, bool compress = true)
        {
            LoadFile();
            string json = JsonConvert.SerializeObject(numberCache.Values);
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            if (compress)
                File.WriteAllBytes(fileName, LZ4Codec.Wrap(StringToByteArray(json)));
            else
                File.WriteAllBytes(fileName, StringToByteArray(json));

        }

        public void SaveFile()
        {

            if (!isChanged) return;

            FileLock.WaitOne();
            IsBusy = true;

            if (!Directory.Exists(Path.GetDirectoryName(Filename)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(Filename));
            }


            IsWriting = true;

            string json = JsonConvert.SerializeObject(numberCache.Values.OrderBy(n => n.NId));


            using (FileStream stream = new FileStream(Filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, 512 * 1024, true))
            //using (var target = new LZ4Stream(stream, LZ4StreamMode.Compress))
            using (var target = K4os.Compression.LZ4.Streams.LZ4Stream.Encode(stream))
            using (var writer = new StreamWriter(target))
            {
                writer.WriteAsync(json).GetAwaiter().GetResult();
            }



            //File.WriteAllBytes(Filename, LZ4Codec.Wrap(StringToByteArray(json)));
            //                File.WriteAllBytes(Filename, StringToByteArray(json));
            lastWrite = DateTime.Now;
            IsWriting = false;

            ConsoleWorker.Log("Saved file: " + Filename + " for " + Tablename + " - amount: " + NumbersCount);

            IsBusy = false;

            FileLock.ReleaseMutex();
        }

        public void HandleLocalFile()
        {
            ConsoleWorker.Log("Starting worker for file: " + Filename);
            IsRunning = true;
            while (IsRunning)
            {
                if (lastWrite < lastUpdate.AddSeconds(-1) && numberCache.Count > 0)
                {
                    SaveFile();
                }

                Thread.Sleep(300);
            }


            lastUpdate = DateTime.Now;
            SaveFile();
            ConsoleWorker.Log("Stopped worker for file: " + Filename);
            GC.Collect();

            IsBusy = false;
        }

    }

    public static class ExceptionHelper
    {
        public static Exception SetCode(this Exception e, int value)
        {
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo fieldInfo = typeof(Exception).GetField("_HResult", flags);

            fieldInfo.SetValue(e, value);

            return e;
        }
    }
}