﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Numerics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Nombro.Objects;
using NPoco;

public class IndexModel : PageModel
{
    [Display(Name = "NumberIsPrime")]
    public BigInteger NumberIsPrime { get; set; }
    public string IsPrimeResult { get; set; }
    public BigInteger AmountNumber { get; set; }
    public BigInteger AmountPrimes { get; set; }
    public BigInteger HighestNumber { get; set; }
    public BigInteger ContinousApproved { get; set; }
    public string Debug { get; set; }
    public List<string> LastTables { get; private set; }
    public List<string> MachineStats { get; private set; }

    public IndexModel(string numberIsPrime = "")
    {
        NumberIsPrime = BigInteger.Parse(numberIsPrime);
        GetLastWriteAccess();
        GetMachineStats();
        GetAmountOfNumbers();
    }

    public void GetMachineStats()
    {
        var db = new Database("server=currentdb;database=Numbers;user id=Numbers;password=1100101;port=3306",
                      DatabaseType.MySQL,
                      MySql.Data.MySqlClient.MySqlClientFactory.Instance);

        using (var transaction = db.GetTransaction(System.Data.IsolationLevel.ReadUncommitted))
        {

            List<MachineStats> stats = db.Query<MachineStats>("select * from AMachineStats ORDER BY LastWrite DESC;").ToList();
            stats = stats.Take(10).ToList();


            MachineStats = stats.Select(t => t.MachineName + " - " + t.FreeRam + " - " + t.UsedCpu + " - " + t.LastWrite.ToShortDateString() + " " + t.LastWrite.ToShortTimeString()).ToList();
            transaction.Complete();
        }
    }


    public void GetLastWriteAccess()
    {
        var db = new Database("server=currentdb;database=Numbers;user id=Numbers;password=1100101;port=3306",
                      DatabaseType.MySQL,
                      MySql.Data.MySqlClient.MySqlClientFactory.Instance);

        using (var transaction = db.GetTransaction(System.Data.IsolationLevel.ReadUncommitted))
        {

            List<TableAccess> tables = db.Query<TableAccess>("select * from ATableAccess ORDER BY LastWrite DESC;").ToList();
            tables = tables.Take(10).ToList();

            LastTables = tables.Select(t => t.TableName + " - " + t.LastWrite.ToShortDateString() + " " + t.LastWrite.ToShortTimeString()).ToList();
            transaction.Complete();
        }
    }

    public void GetAmountOfNumbers()
    {
        var db = new Database("server=currentdb;database=Numbers;user id=Numbers;password=1100101;port=3306",
              DatabaseType.MySQL,
              MySql.Data.MySqlClient.MySqlClientFactory.Instance);


        if (NumberIsPrime > 0)
        {
            var number = GetPrime(NumberIsPrime);
            if (number != null && number.Id != 0)
            {
                if (number.IsPrime)
                {
                    IsPrimeResult = number.Integer + " is a prime.";
                }
                else
                {
                    IsPrimeResult = number.Integer + " is NOT a prime.";
                }

            }
            else
            {
                IsPrimeResult = number.Integer;
            }
        }



        using (var t = db.GetTransaction(System.Data.IsolationLevel.ReadUncommitted))
        {
            AmountNumber = BigInteger.Parse(db.ExecuteScalar<string>("select sum(CountNumbers) from ATableCounts;"));
            AmountPrimes = BigInteger.Parse(db.ExecuteScalar<string>("select sum(CountPrimes) from ATableCounts;"));
            t.Complete();
        }


    }


    private static Number GetPrime(BigInteger bigInteger1)
    {
        var db = new Database("server=currentdb;database=Numbers;user id=Numbers;password=1100101;port=3306",
            DatabaseType.MySQL,
            MySql.Data.MySqlClient.MySqlClientFactory.Instance);


        var id = bigInteger1.ToString();
        string tableName = GetFileByIdentifier(id);

        Number number;
        try
        {
            number = db.FirstOrDefault<Number>("select * from " + tableName + " where `Integer`='" + id + "';");
            if (number == null)
            {
                number = new Number() { Id = -1, Integer = id, IsPrime = false };
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            number =
                new Number()
                {
                    Id = 0,
                    Integer = "Not in database",
                    IsPrime = false,
                };

        }

        return number;
    }


    public static string GetFileByIdentifier(string identifier)
    {

        int IdentifierSplitLength = 6;
        identifier = "         " + identifier;

        string fName = identifier.Substring(0, identifier.Length - IdentifierSplitLength);
        fName = fName.Trim();
        fName = "N" + fName + "M";

        return fName;
    }



}